import React, { Fragment } from 'react'
import 'firebase/auth';
import menu from '../data/menu'
import '../styles/login.css'

const MenuProducts = ({setProductos}) => {
    return ( 
        <Fragment>
            <nav className="main-nav">
                <button className="btn-menu" onClick={() => setProductos(menu.burgers)}>Hamburguesas</button>
                <button className="btn-menu" onClick={() => setProductos(menu.breakfast)}>Desayunos</button>
                <button className="btn-menu" onClick={() => setProductos(menu.drinks)}>Bebidas</button>
                <button className="btn-menu" onClick={() => setProductos(menu.additional)}>Complementos</button>
            </nav>
        </Fragment>
     );
}
 
export default MenuProducts;

