import React, {useState} from 'react';
import Card from './Card'
import Order from './Order'
import MenuProducts from './MenuProducts';
import { useEffect } from 'react';
import menu from "../data/menu";

const Products = () => {
    const [pedido, addProduct] = useState([]);
    const [productos, setProductos] = useState([]);

    useEffect(() => {
        setProductos(menu.burgers)
    }, []);

    return ( 
        <div>
            <MenuProducts setProductos={setProductos} />
            
            <div className="containerMenu">
            
                <div className="containerCard">
                    {productos.map(item => (
                        <Card 
                            key={item.id}
                            producto = {item}
                            addProduct = {addProduct}
                            productos = {productos}
                            pedido = {pedido}
                        />
                    ))} 
                </div>

                <div className="containerOrder">
                    <Order pedido = {pedido}
                        addProduct = {addProduct}
                        productos = {productos}/>
                </div>
            </div>
        </div>
    );
}
 
export default Products;