import React, { useEffect } from 'react';
import {useFirebaseApp} from 'reactfire';
import { useState } from 'react';

const Cookmodule = () => {
    const[ ordenes, setOrdenes] = useState([]);
    const firebase = useFirebaseApp();

    useEffect(() => {
        const db = firebase.firestore();
        let array = []

        db.collection('Orders').where('status', '==', 'iniciada')
            .get().then(function(querySnapshot) {
                querySnapshot.forEach(function(doc) {
                    array.push(doc.data())
                });
                setOrdenes(array)
            }).catch(function(error) {
                console.log("Error getting orders: ", error);
        })
        
    }, []);

    return ( 
        <div className="containerCook">
            
            {ordenes.map((item, index)=> (
                <div className="cardOrder" key={index}>
                    <p><b>Cliente:</b>{item.nombre} </p>
                    <p><b>Mesa:</b>{item.mesa}</p>
           
                <table>
                    <caption>Ticket cocina</caption>
                    
                    <thead>
                        <tr>   
                        <th><p>Cant.</p></th>
                        <th><p>Producto</p></th>
                        </tr> 
                    </thead>
                    
                    {
                        item.productos.map((detalleOrder,index) => (
                        <tbody key={index}>
                            <tr>
                                <td><p>{detalleOrder.quantity}</p></td>
                                <td><p>{detalleOrder.name}</p></td>
                            </tr>
                        </tbody>
                    ))}

                </table>
            </div>
            ))}
        </div>
     );
}
 
export default Cookmodule;